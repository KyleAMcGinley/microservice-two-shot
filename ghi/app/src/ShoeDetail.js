import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

const centeringContainerStyle = {
    "display": "flex",
    "justifyContent": "center",
}

const contentStyle = {
    "display": "flex",
    "justifyContent": "center",
    "width": "18rem",
}

function ShoeDetail() {
    const { id } = useParams()
    const [shoe, setShoe] = useState([]);
    const [bin, setBin] = useState([]);

    const fetchData = async () => {
        const url = `http://localhost:8080/api/shoes/${id}/`;

        const fetchConfig = {
            method: "Get",
            headers: {
                "Content-Type": "application/json"
            }
        };

        const response = await fetch(url, fetchConfig);
        const data = await response.json()
        setBin(data.bin.closet_name)
        setShoe(data)

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div class="centering-container" style={centeringContainerStyle}>
            <div className="content card" style={contentStyle}>
                <img src={shoe.picture_url} alt="" />
                <div className="card-body">
                    <h5 className="card-title">{shoe.manufacturer} {shoe.name}</h5>
                    <p className='card-text'>{shoe.color}</p>
                    <p className='card-text'>{bin}</p>
                </div>
            </div>
        </div>
    )
}

export default ShoeDetail
