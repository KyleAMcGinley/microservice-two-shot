import React, { useEffect, useState } from 'react';


function ShoeForm() {
    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState([]);
    const [name, setName] = useState([]);
    const [color, setColor] = useState([]);
    const [bin, setBin] = useState('');
    const [pictureURL, setPictureURL] = useState([]);


    const handleManufacturerChange = (e) => {
        const value = e.target.value;
        setManufacturer(value);
    }

    const handleNameChange = (e) => {
        const value = e.target.value;
        setName(value);
    }

    const handleColorChange = (e) => {
        const value = e.target.value;
        setColor(value);
    }

    const handleBinChange = (e) => {
        const value = e.target.value;
        setBin(value);
    }

    const handlePictureURLChange = (e) => {
        const value = e.target.value;
        setPictureURL(value);
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.manufacturer = manufacturer;
        data.name = name;
        data.color = color;
        data.bin = bin;
        data.picture_url = pictureURL

        const presentationUrl = `http://localhost:8080/api/shoes/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(presentationUrl, fetchConfig);

        if (response.ok) {
            const newShoe = await response.json();

            setName('');
            setManufacturer('');
            setColor('');
            setBin('');
            setPictureURL('');
        }
    }



    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a shoe</h1>
                    <form onSubmit={handleSubmit} id="create-presentation-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleManufacturerChange} value={manufacturer} placeholder="Manufacturer Name" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                            <label htmlFor="manufacturer">Manufacturer Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Shoe Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Shoe Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleColorChange} value={color} placeholder="Color Name" type="text" name="color" id="color" className="form-control" />
                            <label htmlFor="color">Shoe Color</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleBinChange} value={bin} required name="bin" id="bin" className="form-select">
                                <option value="">Choose a Bin</option>
                                {bins.map((bin) => {
                                    return (
                                        <option value={bin.href} key={bin.id}>
                                            {bin.closet_name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePictureURLChange} value={pictureURL} placeholder="Picture URL" type="url" name="Picture URL" id="Picture URL" className="form-control" />
                            <label htmlFor="Picture URL">Picture URL</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ShoeForm
