// This function returns a form for entering a new hat into
// the database.

import React, {useEffect, useState} from 'react';

function HatForm () {
    const handleSubmit = async (event) => {
        event.preventDefault();

        // Create an empty JSON object
        const data = {};

        // Assign the form data to the JSON object
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;

        // Send the data to the server to save the new hat
        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            // turn the data into a JSON string
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        };

        // Wait for the server response when sending the new hat data
        const hatDataResponse = await fetch(hatUrl, fetchConfig);
        if (hatDataResponse.ok) {
            // If the response was okay, reset the form
            const newHat = await hatDataResponse.json();
            setFabric('');
            setStyleName('');
            setColor('');
            setPictureUrl('');
            setLocation('');
        } else {
            console.log("*******ERROR. Server response: ", hatDataResponse);
        }
    }

    // Via useState, create a variable to hold our list of locations.
    // Set the initial value to an empty string.
    const [locations, setLocations] = useState([]);

    // Set the useState hooks to store the form's values with a default
    // initial value of an empty string.
    const [fabric, setFabric] = useState('');
    const [styleName, setStyleName] = useState('');
    const [color, setColor] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [location, setLocation] = useState('');

    // Create the methods to take what the user inputs
    // into the form and store it in the appropriate state variable.
    const handleFabricChange = (event) => {
        const value = event.target.value;
        setFabric(value);
    }
    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }
    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const fetchData = async () => {
        // Get the list of locations to be displayed on the form for
        // the user to choose from.
        const locationListResponse = await fetch('http://localhost:8100/api/locations/');

        if (locationListResponse.ok) {
            const locationsData = await locationListResponse.json();
            setLocations(locationsData.locations);
        } else {
            console.log("*******ERROR. Server response: ", locationListResponse);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    // Return the JSX/HTML to be rendered on the page.
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h2>Create A New Hat</h2>
                    <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="mb-3">
                            <label htmlFor="fabric">Fabric</label>
                            <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" value={fabric} id="fabric" className="form-control"/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="style">Style</label>
                            <input onChange={handleStyleNameChange} type="text" id="style" name="style" value={styleName} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="color">Color</label>
                            <input onChange={handleColorChange} type="text" id="color" name="color" value={color} className="form-control"/>
                            </div>
                        <div className="mb-3">
                            <label htmlFor="pictureurl" className="form-label">Picture URL</label>
                            <input onChange={handlePictureUrlChange} type="text" className="form-control" name="pictureurl" value={pictureUrl} id="pictureurl" />
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} required id="location" name="location" value={location} className="form-select">
                            <option value="">Choose a closet</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.id} value={location.href}>
                                        {location.closet_name}: Section {location.section_number}, Shelf {location.shelf_number}
                                    </option>
                                )
                            })
                            }
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default HatForm;
