// Display the list of hats and their locations.
import React from 'react';
import ReactDOM from 'react-dom/client';
import {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';

function HatsList() {
    const [hats, setHats] = useState([]);

    const handleSubmit = async (event) => {
        const value = event.target.value;
        // Create Url for specific hat to be deleted
        const hatUrl = `http://localhost:8090/api/hats/${value}`;

        // Format the delete method to send to the server to delete the hat.
        const deleteConfig = {
            method: "delete",
            headers: {
                "Content-Type": "application/json"
            }
       };

       // Send the delete method to the server with the url to the specific
       // hat we will delete.
       const response = await fetch(hatUrl, deleteConfig);
       const data = await response.json();

       setHats(hats.filter(hat => String(hat.id) !== value));
    }

    const getData = async () => {
        // Url for getting the list of hats
        const hatsUrl = "http://localhost:8090/api/hats/";
        // Get the server response
        const response = await fetch(hatsUrl);

        if (response.ok) {
            // Get the list of hats
            const data = await response.json();
            setHats(data.hats)
        }
    }

    useEffect(()=>{
        getData()
    }, [])

    return (
        <>
            <h1>Your Hats</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Color</th>
                        <th>URL</th>
                        <th>Closet</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map(hat => {
                    return (
                        <tr key={hat.id}>
                            <td>{ hat.fabric }</td>
                            <td>{ hat.style_name }</td>
                            <td>{ hat.color }</td>
                            <td>{ hat.picture_url }</td>
                            <td>{ hat.location.closet_name }</td>
                            <td>
                                <button onClick={handleSubmit} value={hat.id} className="btn btn-danger">Delete</button>
                            </td>
                        </tr>
                    )
                    })}
                </tbody>
            </table>
            <Link to="/hats/new"><button type="button" className="btn btn-primary">Add a Hat</button></Link>
        </>
    );
}

export default HatsList;
