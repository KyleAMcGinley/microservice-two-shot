import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';


function ShoesList() {
    const [shoes, setShoes] = useState([]);

    const handleDelete = async (e) => {
        const value = e.target.value;
        const url = `http://localhost:8080/api/shoes/${value}/`

        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const response = await fetch(url, fetchConfigs)
        const data = await response.json()

        setShoes(shoes.filter(shoe => String(shoe.id) !== value))

    }


    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes)
        }
    }


    useEffect(() => {
        fetchData();
    }, []);


    return (
        <div className="content">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Name</th>
                        <th>Color</th>
                        <th>Bin</th>
                        <th>Picture URL</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                        return (
                            <tr key={shoe.id}>
                                <td>{shoe.manufacturer}</td>
                                <td><Link to={`/shoes/${shoe.id}`}>{shoe.name}</Link></td>
                                <td>{shoe.color}</td>
                                <td>{shoe.bin.closet_name}</td>
                                {(() => {
                                    if (shoe.picture_url === null) {
                                        return (
                                            <td>Link</td>
                                        )
                                    } else {
                                        return (
                                            <td><a href={shoe.picture_url}>Link</a></td>
                                        )
                                    }
                                })()}
                                <td><button onClick={handleDelete} value={shoe.id} className="btn btn-danger">Delete</button></td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
            <Link to="/shoes/new"><button type="button" className="btn btn-primary">Add Shoe</button></Link>
        </div>
    )

}

export default ShoesList
