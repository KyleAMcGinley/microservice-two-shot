from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO

# Create your views here.

class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name"
    ]

# Include the ID so it is easy to delete a hat instance.
class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_hats(request):
    """
    GET: returns the list of hats and their details.
    POST: creates a new instance of the hat model.
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        new_hat = json.loads(request.body)
        try:
            # Get the location object and put it in the new_hat data to be
            # saved to the server
            # Using the href, because the id on the actual Location object may not
            # match the id on the LocationVO object. The href, however, will always
            # match.
            location = LocationVO.objects.get(import_href=new_hat["location"])
            # Store that location in the new hat's location property
            new_hat["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )

        # Send the new hat object to the database.
        hat = Hat.objects.create(**new_hat)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET"])
def api_hat(request, pk):
    """
    DELETE: deletes a hat instance.
    """
    if request.method == "DELETE":
        count, _ = Hat.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count >0})
    else:
        try:
            # Get the details for the specific hat
            hat = Hat.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Hat does not exist"})
            response.status_code = 404
            return response
