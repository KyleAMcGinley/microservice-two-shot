# Wardrobify

Team:

* Kyle - Shoes
* Lotus - Hats

## Design

## Shoes microservice
Created a Shoe model class and a BinVO model class to access the Bin class in the wardrobe app.
Explain your models and integration with the wardrobe

## Hats microservice
LocationVO: Contains an href and the closet name where the hat is located.
Hat: the model for instances of hats.

Functionality: Hats can be listed, added, and deleted.
